import tkinter

from cv2 import CAP_V4L

WIDTH = 1024
HEIGHT = 768
FPS = 30
G = 6.67

points = []
stars = []

win = tkinter.Tk()
CANVAS = tkinter.Canvas(win, width=WIDTH, height=HEIGHT, background="black")
CANVAS.pack()

start_weight = tkinter.Entry(win)
start_weight.pack()

def place_points(event):
    Point(event.x, event.y)


def place_star(event):
    start_field_len = len(start_weight.get())
    try:
        weight = float(start_weight.get())
        Star(event.x, event.y, weight)
    except Exception:
        start_weight.delete(0, start_field_len)


CANVAS.bind("<B1-Motion>", place_points)
CANVAS.bind("<Button-3>", place_star)

def clear_points():
    points.clear()

clear_button = tkinter.Button(win, text="Clear", command=clear_points)
clear_button.pack()

def clear_all():
    points.clear()
    stars.clear()

clear_button = tkinter.Button(win, text="Clear All", command=clear_all)
clear_button.pack()


def get_e_vector(from_object, to_object):
    x = to_object.x - from_object.x
    y = to_object.y - from_object.y
    length = (x*x+y*y)**0.5
    return (x/length, y/length)

class DrawObject:
    color = "white"
    weigth = 1
    rad =  2

    def __init__(self, x, y, x_speed=0, y_speed=0):
        self.x = x
        self.y = y
        self.x_speed = x_speed
        self.y_speed = y_speed

    def draw(self):
        CANVAS.create_oval(self.x-self.rad, self.y-self.rad, self.x+self.rad, self.y+self.rad, fill=self.color)

    def tick(self):
        self.draw()

class Point(DrawObject):
    def __init__(self, x, y, x_speed=2, y_speed=0):
        points.append(self)
        super().__init__(x, y, x_speed, y_speed)

    def calculate_speed(self):
        for star in stars:
            r_square = (self.x - star.x)**2 + (self.y - star.y)**2
            force = G * star.weigth * self.weigth / r_square
            x_e_vector, y_e_vector = get_e_vector(self, star)
            self.x_speed += x_e_vector * force / self.weigth
            self.y_speed += y_e_vector * force / self.weigth

    def move(self):
        self.calculate_speed()
        self.x += self.x_speed
        self.y += self.y_speed

    def tick(self):
        self.move()
        return super().tick()        

class Star(DrawObject):
    def __init__(self, x, y, weigth, x_speed=0, y_speed=0):
        self.color = "red"
        self.weigth = weigth
        self.rad = weigth / 200
        stars.append(self)

        super().__init__(x, y, x_speed, y_speed)

Star(WIDTH/2, HEIGHT/2, 1000)

def tick():
    CANVAS.delete("all")
    
    for star in stars:
        star.tick()

    for point in points:
        point.tick()

    win.after(1000//FPS, tick)

tick()

win.mainloop()